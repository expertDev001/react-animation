import React, { Component } from 'react';
import Popup from './Popup';

class Tiles extends Component {

  constructor() {
    super();
    this.state = {
      animate:false
    }
  }

  animate = (data) => {
  	this.setState({animate:!this.state.animate})
 }

render(){ 
  const { id, height, backgroundColor } = this.props;
  const { animate } = this.state;
  return (
  	<div className="">
        <div onClick={() => this.animate(id)} className={'tile'} style={{height:height, backgroundColor:backgroundColor}} />
   		{ animate ? <Popup animate={this.animate} id={id} backgroundColor={backgroundColor}/> : '' }
   </div>
  );
 }
};

export default Tiles;