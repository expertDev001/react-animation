import React, { Component } from 'react';
import './App.css';
import Tile from './Tiles';

class Masonry extends Component {

  constructor() {
    super();
    this.state = {
      columns:[1,2,3,4,5,6,7],
      open:false,
    }
  }

  getTiles = () => {
    let tiles = [];

    for( let i = 1; i < 7 ; i++) {

      let backgroundColor = this.getRandomColor();
      let height = this.getrandomHeight();

       tiles.push({height, backgroundColor})
     };

     return tiles;
  }

 getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
 }

 getrandomHeight = () => {
   let randomHeight = Math.floor(Math.random() * 300) + 100  
   return randomHeight;
 }

 animate = (data) => {

 }

  render() {
     let inc = 0;
    return (
        <div className="masonry">
         { this.state.columns.map((col, i) => {
            return (
             <div key={i} className="column">
                { this.getTiles().map((data, i1) => {
                  inc = inc + 1;
                   return (
                        <Tile {...this.props} animate={this.animate} key={i1} id={inc} height={data.height} backgroundColor={data.backgroundColor} />
                      ) 
                    }) 
                 }
               </div>
             )
            })
           }
        </div>
    );
  }
}

export default Masonry;


