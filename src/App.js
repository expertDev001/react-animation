import React, { Component } from 'react';
import './App.css';
import Masonry from './Masonry';

class App extends Component {
  render() {
    return (
        <div className="masonry-container">
           <Masonry {...this.props}/>
        </div>
    );
  }
}

export default App;


