import React, { Component } from 'react';
import './App.css';

class Popup extends Component {

  constructor() {
    super();
    this.state = {
      show:false
    }
  }

  animate = () =>  {
      this.props.animate();
  }

  getRandomColor = () => {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
 }

  componentDidMount = () =>{
      setTimeout( () => { 
        this.setState({
          show:true
        })
      }, 100);
  }
render(){ 
  let { backgroundColor } = this.props;
  if(!backgroundColor) {
    backgroundColor = this.getRandomColor();
  }
  return (
  	<div onClick={this.animate} className="popup"  >   
      <div className={this.state.show? 'popup_inner animate' : 'popup_inner '}  style={{backgroundColor}}></div>
   </div>
  );
 }
};

export default Popup;