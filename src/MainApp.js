import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import App from './App';
import Popup from './Popup';

const MainApp = () =>
    <Router>
	    <Switch>
	        <Route exact path="/"  render={(props) => <App {...props} /> }/>
	        <Route exact path="/open/:id" render={(props) => <Popup {...props} /> } />
	    </Switch>
	</Router>;


export default MainApp;